package distributedserver;

import java.io.Serializable;

@SuppressWarnings("serial")
class DistributedServerDataUnit implements Serializable
{
	private final String serverIP;
	private final long value;
	private final String senderIP;
	
	public DistributedServerDataUnit(String serverIP, long value, String senderIP)
	{
		this.serverIP = serverIP;
		this.value = value;
		this.senderIP = senderIP;
	}

	public String getServerIP() 
	{
		return this.serverIP;
	}

	public long getValue() 
	{
		return this.value;
	}

	public String getSenderIP() 
	{
		return this.senderIP;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(this == obj) return true;
		if(obj == null) return false;
		if(this.getClass() != obj.getClass()) return false;
		
		DistributedServerDataUnit dataUnit = (DistributedServerDataUnit)obj;
		return this.serverIP == dataUnit.serverIP && this.value == dataUnit.value && this.senderIP == dataUnit.senderIP;
	}
	
	@Override
	public String toString() 
	{
		return "{" + this.serverIP + ", " + this.value + ", " + this.senderIP + "}";
	}
}