package distributedserver;

import java.lang.Thread.State;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class DistributedServer 
{
	private final List<DistributedServerDataUnit> playerDataUnits;
	private final long value;
	private final String currentIP;
	protected long startTime;
	protected boolean firstDelivery;
	protected Thread sendThread;
	protected Thread receiveThread;
	
	public DistributedServer() throws UnknownHostException
	{
		this.playerDataUnits = new ArrayList<DistributedServerDataUnit>();
		this.value = Math.abs(new Random().nextLong());
		this.currentIP = InetAddress.getLocalHost().getHostAddress();
		this.startTime = 0;
		this.firstDelivery = false;
		this.sendThread = new Thread(() -> this.sendDataUnit());
		this.receiveThread = new Thread(() -> this.receiveDataUnit());
	}
	
	public void find()
	{
		if(this.sendThread.getState() == State.NEW && this.receiveThread.getState() == State.NEW)
		{
			System.out.println(this.getClass().getName() + ": ready to find distributed server " + this.currentIP);
			
			this.startTime = System.currentTimeMillis();
			this.firstDelivery = true;
			
			System.out.println(this.getClass().getName() + ": initial time " + (this.startTime / 1000));
			
			this.sendThread.start();
			this.receiveThread.start();
		}
	}
	
	protected void sendDataUnit()
	{
	}
	
	protected void receiveDataUnit()
	{
	}
}
